# klm

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.16.0.

1.Install nodejs.
2.Install bower via npm(globally or local for project).
3.Install grunt via npm(globally or local for project)
3.Go the command prompt of the project folder and give the following commands

	a. npm install
	b. bower install

By the above command all the dependencies of the projects will be added to the project folder

4. Finally run "grunt serve" command to open up the application in the default browser.
