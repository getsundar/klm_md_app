angular.module('klmApp')

.directive('employeeChatDetails', function() {
        return {
            restrict: 'E',
            scope: {
                emp: '=',
                currentModule: '@'
            },
            controller: ['$scope', function($scope) {}],
            templateUrl: "views/templates/employeeChatDetails.html"
        };
    })
    .directive('employeeFollowedDetails', function() {
        return {
            restrict: 'E',
            scope: {
                emp: '=',
                currentModule: '@'
            },
            controller: ['$scope', function($scope) {}],
            templateUrl: "views/templates/employeeFollowed.html"
        };
    })
    .directive('sideNavigation', function() {
        return {
            restrict: 'E',
            templateUrl: "views/templates/sideNav.html"
        };
    })
    .directive('mainContent', function() {
        return {
            restrict: 'E',
            templateUrl: "views/templates/mainContent.html"
        };
    })
