angular.module('klmApp')
    .filter('unreadChatsFilters', function() {
        return function(chats) {
            var filtered = [];
            angular.forEach(chats, function(chat) {
                if (chat.chats.length != 0 && chat.chats[0].chatUnread) {
                    filtered.push(chat);
                }
            });
            return filtered;
        }
    })

.filter('readChatsFilters', function() {
    return function(chats) {
        var filtered = [];
        angular.forEach(chats, function(chat) {
            if (chat.chats.length != 0 && !chat.chats[0].chatUnread) {
                filtered.push(chat);
            }
        });
        return filtered;
    }
});
