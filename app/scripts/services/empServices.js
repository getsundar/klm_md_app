angular.module('klmApp')
    .factory('httpInterceptor', ['$rootScope', function($rootScope) {
        return {
            request: function(config) {
                //$rootScope.$broadcast('loading:show');
                //config.timeout = 100000;                
                return config;
            },
            response: function(response) {
                $rootScope.loaded = true;
                return response;
            },

            requestError: function(rejection) {
                console.log('ERR:' + JSON.stringify(rejection));
                //$rootScope.$broadcast('loading:hide');

            },
            responseError: function(rejection) {
                //$rootScope.$broadcast('loading:hide');
                console.log('ERR:' + JSON.stringify(rejection));
            }
        };
    }])
    .service('employeeServices', ['$http', function($http) {

        this.getEmployeeDetails = function($q) {
            var deferred = $q.defer();
            $http.get("data/employeesDetails.json")
                .then(function(response) {
                    deferred.resolve(response.data);
                });

            return deferred.promise;
        };


    }]);
