'use strict';

/**
 * @ngdoc overview
 * @name klmApp
 * @description
 * # klmApp
 *
 * Main module of the application.
 */
angular
    .module('klmApp', [
        'ngAnimate',
        'ngAria',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ui.router',
        'ngMaterial'
    ])
    .config(function($stateProvider, $urlRouterProvider,$mdThemingProvider,$mdIconProvider) {

        $stateProvider
            .state('home', {
                name: 'home',
                url: '/home',
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                controllerAs: 'main',
                resolve: {
                    resolvedData: function(employeeServices, $q) {
                        var data = employeeServices.getEmployeeDetails($q);
                        return data;
                    }
                }
            })
            .state('employeeChats', {
                parent: 'home',
                name: 'employeeChats',
                url: '/employeeChats',
                templateUrl: 'views/templates/employeeChats.html',
                /*controller: 'HeroesDashboardCtrl',
                controllerAs: 'HeroesDashboard'*/
            })
            .state('employeesFollowed', {
                parent: 'home',
                name: 'employeesFollowed',
                url: '/employeesFollowed',
                templateUrl: 'views/templates/employeeFollowed.html',
                /*controller: 'HeroesListCtrl',
                controllerAs: 'HeroesList'*/
            });           
        $urlRouterProvider.otherwise('/home/employeeChats');
        $mdThemingProvider.theme('default')    
        .accentPalette('blue-grey');
        $mdIconProvider.icon("avatar", "assets/avatar.svg")
    });
