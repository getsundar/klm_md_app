'use strict';

/**
 * @ngdoc function
 * @name klmApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the klmApp
 */
angular.module('klmApp')
    .controller('MainCtrl', ['$scope', '$http', '$filter', 'employeeServices', '$state', '$q', 'resolvedData', '$mdSidenav', function($scope, $http, $filter, employeeServices, $state, $q, resolvedData, $mdSidenav) {
        $scope.empDetails = resolvedData;
        $scope.currentModule = $state.$current.name;
        $scope.moveto = function(stateToMove, heroId) {
            this.currentModule = stateToMove;
            $state.go(stateToMove, { heroId: heroId });
        }
        $scope.openSideNav = function(componentId) {
            $mdSidenav(componentId).toggle();
        }


    }])

.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('httpInterceptor');
}]);
